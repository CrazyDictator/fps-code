﻿using UnityEngine;

public class FireAction : MonoBehaviour {

	public float Damage = 10f;
	public float Range = 1000f;
	public GameObject BarrelEnd;
	public float BulletVelocity;
    public Animator anm;
    public GameObject TestOrb;
    public float ScopedOutFireRate;
    public float ScopedInFireRate;

    private Vector3 target;
   


    float nextTimeToFire;

	void Start()
    {
        anm = GetComponent<Animator>();
    }
	
	void Update() {
		if(Input.GetButton("Fire1") && Time.time >= nextTimeToFire && !anm.GetBool("IsWalking")) {

            
			Shoot();

            if (!anm.GetBool("ScopedIn"))
            {
                anm.SetTrigger("ScopedOutShot");
                nextTimeToFire = Time.time + 1f / ScopedOutFireRate;
            } else
            {
                anm.SetTrigger("ScopedInShot");
                nextTimeToFire = Time.time + 1f / ScopedInFireRate;
            }

		}
	}


	void Shoot() {
       
        RaycastHit hit;

            target = Camera.main.transform.position;

        
		
		if(Physics.Raycast(target, BarrelEnd.transform.forward, out hit, Range)) {

			Debug.Log(hit.transform.gameObject);
			if(hit.transform.gameObject.tag == "Terrain") {
				//Play DirtHitPS
			} else if(hit.transform.gameObject.tag == "PracticeTarget") {
				Debug.Log("Hit Practice");
                PracticeHit ph = hit.transform.gameObject.GetComponent<PracticeHit>();
                    if(ph != null)
                    ph.GetShot(TravelTime(hit.point, BarrelEnd.transform.position), Damage, hit.normal);
			}

            Instantiate(TestOrb, hit.point, Quaternion.identity);

		}
	}

	float TravelTime(Vector3 hitPoint, Vector3 exitPoint) {
		float top = hitPoint.x - exitPoint.x;
		Debug.Log(Mathf.Clamp(top / BulletVelocity, 1f, Mathf.Infinity));

		return Mathf.Clamp(top / BulletVelocity, 1f, Mathf.Infinity);	
	}
}
