﻿using System.Collections;
using UnityEngine;

public class PracticeHit : MonoBehaviour {
    
    public void GetShot(float Time, float Damage, Vector3 hitPoint)
    {
        StartCoroutine(ApplyDamage(Time, Damage, hitPoint));
    }

    IEnumerator ApplyDamage(float Time, float Damage, Vector3 hitPoint)
    {
        yield return new WaitForSeconds(Time);

        GetComponent<Rigidbody>().AddForce(-hitPoint * Damage);
    }

}
