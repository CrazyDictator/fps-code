﻿using UnityEngine;

public class ScopeZoomInContrl : MonoBehaviour {

    public Camera MainCam;
    public GameObject Weapon;
    public float MaxZoomValue, MinZoomValue, Sensitivity;
    float StartFov;

    void Start()
    {
        StartFov = MainCam.fieldOfView;
    }
    

    void Update()
    {
        float sop = MainCam.fieldOfView;

        if (Weapon.GetComponent<ScopeInOut>().isScop)
        {
            sop += Input.GetAxis("Mouse ScrollWheel") * -Sensitivity;
        } else
        {
            sop = StartFov;
        }

        sop = Mathf.Clamp(sop, MinZoomValue, MaxZoomValue);

        MainCam.fieldOfView = sop;
    }
    
}
