﻿using UnityEngine;
using UnityEngine.UI;

public class ScopeInOut : MonoBehaviour {

	Animator anm;
    public Image InScopeCH;

	void Start () {
		anm = GetComponent<Animator> ();
	}

	public bool isScop = false;

	void Update () {
		if (Input.GetButtonDown("Fire2") && !anm.GetBool("IsWalking"))
        {
			
			isScop = !isScop;
            
            InScopeCH.gameObject.SetActive(!isScop);
			anm.SetBool ("ScopedIn", isScop);
            
		} 

	}
}
