﻿using UnityEngine;

public class CameraTurning : MonoBehaviour {

	Vector2 mouseLook;
	Vector2 smoothN;

	public float Sensitivity = 3;
	public float smoothing = 2;

	GameObject Player;

	void Start () {
		Player = this.transform.parent.gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 md = new Vector2 (Input.GetAxisRaw ("Mouse X"), Input.GetAxisRaw ("Mouse Y"));

		md = Vector2.Scale(md, new Vector2(Sensitivity * smoothing, Sensitivity * smoothing));

		smoothN.x = Mathf.Lerp (smoothN.x, md.x, 1f / smoothing);
		smoothN.y = Mathf.Lerp (smoothN.y, md.y, 1f / smoothing);

		mouseLook += smoothN;
		mouseLook.y = Mathf.Clamp (mouseLook.y, -90, 90);

		transform.localRotation = Quaternion.AngleAxis (-mouseLook.y, Vector3.right);
		Player.transform.localRotation = Quaternion.AngleAxis (mouseLook.x, Player.transform.up);
	}
}
