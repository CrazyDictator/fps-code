﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	public float WalkbackSpeed = 3.5f;
	public float WalkSpeed = 5;
	public float StraffeSpeed = 2;
	Vector3 tranl;
    public Animator GunAnim;


	// Use this for initialization
	void Start () {
		Cursor.lockState = CursorLockMode.Locked;


	}
	
	// Update is called once per frame
	void Update () {
		float h = Input.GetAxis ("Horizontal");
		float v = Input.GetAxis ("Vertical");



		if (v > 0) {
			tranl.z = v * WalkSpeed;
			tranl.x = h * StraffeSpeed;
            GunAnim.SetBool("IsWalking", true);
		} else if (v < 0) {
			tranl.z = v * WalkbackSpeed;
			tranl.x = h * StraffeSpeed;
            GunAnim.SetBool("IsWalking", true);
        } else {
			tranl.z = v;
			tranl.x = h * StraffeSpeed;
            GunAnim.SetBool("IsWalking", false);
        }

        
		transform.Translate (tranl * Time.deltaTime);

		if (Input.GetKey(KeyCode.LeftControl)) {
			Cursor.lockState = CursorLockMode.None;
		} else {
			Cursor.lockState = CursorLockMode.Locked;
		}

	}
}
